const express = require("express")

const app = express()

// 定义全局tooken密钥
app.set("secret","qwertyuioplkjhgfdsazxcvbnm")

app.use(require("cors")())
app.use(express.json())
app.use("/admin/",express.static(__dirname + "/admin"))
app.use("/",express.static(__dirname + "/web"))
app.use("/uploads",express.static(__dirname + "/uploads"))

require("./plugins/db")(app)
require("./routes/admin")(app)
require("./routes/web")(app)

app.listen(3000, (req,res) =>{
    console.log("http://localhost:3000")
})