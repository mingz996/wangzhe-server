const mongoose = require("mongoose")


const schema = new mongoose.Schema({
    // 英雄名称
    name: {
        type: String
    },
    // 英雄头像
    avater: {
        type: String
    },
    // 背景图
    banner: {
        type: String
    },
    // 英雄称号
    title: {
        type: String
    },
    // 英雄职业 为了多选用复数
    categories: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Category"
    }],
    // 英雄操作等级划分
    scores: {
        // 上手难度
        difficult: {
            type: Number
        },
        // 技能
        skills: {
            type: Number
        },
        // 攻击
        attack: {
            type: Number
        },
        // 生存
        survive: {
            type: Number
        },
    },
    // 技能
    skills: [{
        // 技能图标
        icon: {
            type: String
        },
        // 冷却
        delay: {
            type: String
        },
        // 消耗
        cost: {
            type: String
        },
        // 技能名称
        name: {
            type: String
        },
        //技能描述 
        description: {
            type: String
        },
        // 小提示
        tips: {
            type: String
        }
    }],
    // 顺风出装
    items1: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Item'
    }],
    // 逆风出装
    items2: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Item'
    }],
    // 推荐铭文
    mingwen: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Item"
    }],
    // 使用技巧
    usageTips: {
        type: String
    },
    // 对抗技巧
    battleTips: {
        type: String
    },
    // 团战技巧
    teamTips: {
        type: String
    },
    // 英雄关系 最佳搭档
    parteners: [{
        // 英雄头像
        hero: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Hero'
        },
        // 描述
        description: {
            type: String
        }
    }],
    // 被谁克制
    restraint: [{
        // 英雄头像
        hero: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Hero'
        },
        // 描述
        description: {
            type: String
        }
    }],
    // 克制谁
    enemy: [{
        // 英雄头像
        hero: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Hero'
        },
        // 描述
        description: {
            type: String
        }
    }]



})

module.exports = mongoose.model("Hero", schema, "heroes")