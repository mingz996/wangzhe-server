const mongoose = require("mongoose")


const schema = new mongoose.Schema({
    // 文章标题
    title: {type: String},
    // 文章类型 为了多选用复数
    categories: [{type: mongoose.SchemaTypes.ObjectId,ref:"Category"}], 
    // 文章详情
    body: {type: {String}},
    // 文章浏览量
    num : {type: {Number}}
},{
    timestamps: true
})

module.exports = mongoose.model("Article",schema)