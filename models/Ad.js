const mongoose = require("mongoose")


const schema = new mongoose.Schema({
    // 广告标题
    title: {type: String},
    // 广告位所含的信息
    items: [{
        image: {type: String},
        url: {type: String}
    }]
})

module.exports = mongoose.model("Ad",schema)