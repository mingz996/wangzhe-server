module.exports = options =>{
    const assert = require("http-assert")
    const jwt = require("jsonwebtoken")
    const AdminUser = require("../models/AdminUser")

    return async(req,res,next) =>{
        const token = String(req.headers.authorization || " ").split(" ").pop()
        assert(token,401,"未登录")
        const {id} = jwt.verify(token,req.app.get("secret"))
        assert(id,401,"请重新登录")
        req.user = await AdminUser.findById(id)
        assert(req.user,401,"用户不存在,请重新登录")
        await next()
    } 
}